const Sequelize = require('sequelize');
const db = require('../db.js');

const Student = db.define('student', {
    id: {
		type: Sequelize.INTEGER,
		primaryKey: true,
		autoIncrement: true,
    },
    firstName: {
		type: Sequelize.STRING,
		allowNull: false
    },
    lastName: {
		type: Sequelize.STRING,
		allowNull: false,
    },
});

module.exports = Student;
