const Sequelize = require('sequelize');
const db = require('../db.js');

const Student = require('./student.js');
const Assignment = require('./grade.js');

const Grade = db.define('grade', {
    id: {
	primaryKey: true,
	autoIncrement: true,
	type: Sequelize.INTEGER,
    },
    symbol: {
	type: Sequelize.STRING,
	allowNull: false,
    },
    score: {
	type: Sequelize.INTEGER,
	allowNull: false,
    },
    //assignment: Assignment;
    //author: Student;
});

module.exports = Grade;
