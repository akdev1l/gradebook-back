const Sequelize = require('sequelize');
const db = require('../db.js');

const Grade = require('./grade.js');

const AssignmentType = Sequelize.ENUM(
    'Assignment',
    'Test'
);

const Assignment = db.define('assignment', {
    id: {
	primaryKey: true,
	allowNull: false,
	autoIncrement: true,
	type: Sequelize.INTEGER
    },
    type: {
	type: AssignmentType,
	allowNull: false,
    },
    description: {
	type: Sequelize.STRING,
	allowNull: false,
    },
    weight: {
	type: Sequelize.INTEGER,
	allowNull: false,
    },
});

module.exports = Assignment;
