const db = require('../db.js');
const Grade = require('./grade.js');
const Student = require('./student.js');
const Assignment = require('./assignment.js');


Grade.belongsTo(Student, { as: 'author', foreignKey: 'student_id' });
Grade.belongsTo(Assignment, { as: 'assignment', foreignKey: 'assignment_id' });


Assignment.belongsToMany(Student, { as: 'authors', foreignKey: 'student_id', through: 'grades' });
Student.hasMany(Grade, { as: 'grades', foreignKey: 'student_id', onDelete: 'cascade' });
Assignment.hasMany(Grade, { as: 'grades', foreignKey: 'assignment_id' });

module.exports = {
    Grade: Grade,
    Student: Student,
    Assignment: Assignment,
};
