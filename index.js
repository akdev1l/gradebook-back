const express = require('express');
const db = require('./db.js');
const bodyParser = require('body-parser');

const cors = require('cors');
const app = express();
const entities = require('./entities');
const Student = entities.Student;
const Grade = entities.Grade;
const Assignment = entities.Assignment;

const PORT = process.env.PORT;

const fromScore = (score) => {
    if(score < 50) return 'F';
    if(score < 60) return 'D';
    if(score < 70) return 'C';
    if(score < 80) return 'B';
    if(score < 90) return 'A';

    return 'A+';
}

const average = (grades) => {
    const averageScore = grades
    .map(grade => grade.score)
    .reduce((prev, curr) => prev + curr, 0) / grades.length;
    const average = {
        symbol: fromScore(averageScore),
        score: averageScore
    };

    return average;
}
const averageStudent = (student) => {
    return {
        firstName: student.firstName,
        lastName: student.lastName,
        id: student.id,
        grades: student.grades,
        average: average(student.grades),
    };
};

const averageAssignment = (assignment) => {
    return {
        type: assignment.type,
        id: assignment.id,
        grades: assignment.grades,
        description: assignment.description,
        weight: assignment.weight,
        average: average(assignment.grades),
    }
};


app.use(bodyParser.json());
app.use(cors());
app.get('/api/students', (req, res) => {
    Student.findAll({
        include: [{
            model: Grade,
            as: 'grades'
        }]
    }).then(students => {
        const studentsWithAverage = students.map(student => averageStudent(student));
        res.json(studentsWithAverage);
    });
});

app.get('/api/student/:id', (req, res) => {
    Student.findById(req.params.id, {
        include: [
        {
            model: Grade,
            as: 'grades',
            include: [{
                model: Assignment,
                as: 'assignment',
            }]
        }, 
    ]
    }).then(student => {
            console.log(`${student}`);
        if(!!student) {
            student = averageStudent(student);
            res.json(student);
        }
        else {
            res.status(404).send('Not found');
        }
    })
    .catch(err => {
        console.error(`error: ${JSON.stringify(err)}`); 
        res.status(404).send('Not found');
    });
});

app.delete('/api/student/:id', (req, res) => {
    const student = req.body;
    student.id = req.params.id;
    Grade.destroy({
        where: {
            student_id: student.id
        }
    }).then(() => {
        Student.destroy({
            where: {
                id: student.id,
            },
            cascade: true
        }).then(() => {
            res.status(200);
            res.send();
        });
    })
});

app.post('/api/student/create', (req, res) => {
    const student = req.body;
    Student.create(student).then(student => {
        res.json(student);
    });
})

app.patch('/api/student', (req, res) => {
    const student = req.body;
    Student.update(student, {
        where: {
            id: student.id,
        }
    }).then(() => {
        res.status(200);
        res.send();
    });
});

// Assignment Routes

app.get('/api/assignments', (req, res) => {
    Assignment.findAll({
        include: [{
            model: Grade,
            as: 'grades',
            include: [{
                model: Student,
                as: 'author'
            }, {
                model: Assignment,
                as: 'assignment',
            }],
        }]
    }).then(assignments => {
        assignments = assignments.map(assignment => averageAssignment(assignment));
        res.json(assignments);
    })
})

app.get('/api/assignment/:id', (req, res) => {
    Assignment.findById(req.params.id, {
        include: [{
            model: Grade,
            as: 'grades',
            include: [{
                model: Student,
                as: 'author',
            }]
        }]
    })
    .then(assignment => {
        assignment = averageAssignment(assignment);
        res.json(assignment);
    });
});

app.post('/api/assignment/:id/grades/add', (req, res) => {
    const grade = req.body;
    grade.assignment_id = req.params.id;
    grade.student_id = grade.author;
    grade.symbol = fromScore(grade.score);
    Grade.create(grade).then(newGrade => {
        Grade.findAll({
            where: {
                assignment_id: req.params.id
            },
            include: {
                model: Student,
                as: 'author',
            }
        }).then(newGrades => {
            res.json(newGrades);
        });
    });
});

app.delete('/api/assignment/:id', (req, res) => {
    Assignment.destroy({
        where: {
            id: req.params.id,
        },
        cascade: true,
    }).then(() => {
        res.status(200);
        res.send();
    });
});

app.get('/api/grade/:id', (req, res) => {
    Grade.findById(req.params.id, {
        include: [{
            model: Student,
            as: 'author'
        }]
    }).then(grade => {
        res.json(grade);
    });
})

app.patch('/api/grade', (req, res) => {
    const grade = req.body;
    grade.symbol = fromScore(grade.score);
    Grade.update(grade, {
        where: {
            id: grade.id,
        },
        cascade: true,
    }).then(() => {
        res.status(200);
        res.send();
    });
})

app.listen(PORT,
   () => console.log(`Gradebook listening at http://localhost:${PORT}`));
