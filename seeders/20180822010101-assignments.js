'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('Person', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
      return queryInterface.bulkInsert('assignments', [
        {
          description: 'Written Essay',
          weight: 100,
          type: 'Assignment',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          description: 'Video Essay',
          weight: 100,
          type: 'Assignment',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          description: 'Written Test',
          weight: 100,
          createdAt: new Date(),
          updatedAt: new Date(),
          type: 'Test',
        },
      ])
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
      return queryInterface.bulkDelete('assignments', null, {});
  }
};
