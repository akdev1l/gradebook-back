'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('Person', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
   return queryInterface.bulkInsert('grades', [
     {
       score: 80,
       symbol: 'A',
       createdAt: new Date(),
       updatedAt: new Date(),
       student_id: 1,
       assignment_id: 1,
     },
     {
       score: 80,
       symbol: 'A',
       createdAt: new Date(),
       updatedAt: new Date(),
       student_id: 2,
       assignment_id: 2,
     },
     {
       score: 80,
       symbol: 'A',
       createdAt: new Date(),
       updatedAt: new Date(),
       student_id: 1,
       assignment_id: 3,
     },
   ])
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
      return queryInterface.bulkDelete('grades', null, {});
  }
};
